package resolution.example6.zzeulki.homework911;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        WebView mWebView = (WebView)findViewById(R.id.WebView1);
       // mWebViewClient(new WebViewClient());
        mWebView.loadUrl("http://m.naver.com");

        mWebView.setWebViewClient(new WebViewClient());

    }
}
